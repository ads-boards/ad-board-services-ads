"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdsProcess = void 0;
const pip_services3_container_node_1 = require("pip-services3-container-node");
const AdsServiceFactory_1 = require("../build/AdsServiceFactory");
const pip_services3_rpc_node_1 = require("pip-services3-rpc-node");
const pip_services3_grpc_node_1 = require("pip-services3-grpc-node");
class AdsProcess extends pip_services3_container_node_1.ProcessContainer {
    constructor() {
        super("ads-library", "Ads microservice");
        this._factories.add(new AdsServiceFactory_1.AdsServiceFactory);
        this._factories.add(new pip_services3_rpc_node_1.DefaultRpcFactory);
        this._factories.add(new pip_services3_grpc_node_1.DefaultGrpcFactory);
    }
}
exports.AdsProcess = AdsProcess;
//# sourceMappingURL=AdsProcess.js.map