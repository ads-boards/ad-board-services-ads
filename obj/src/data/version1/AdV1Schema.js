"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdV1Schema = void 0;
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_commons_node_2 = require("pip-services3-commons-node");
const pip_services3_commons_node_3 = require("pip-services3-commons-node");
class AdV1Schema extends pip_services3_commons_node_1.ObjectSchema {
    constructor() {
        super();
        this.withOptionalProperty('id', pip_services3_commons_node_3.TypeCode.String);
        this.withRequiredProperty('publish_group_ids', new pip_services3_commons_node_2.ArraySchema(pip_services3_commons_node_3.TypeCode.String));
        this.withRequiredProperty('title', pip_services3_commons_node_3.TypeCode.String);
        this.withRequiredProperty('text', pip_services3_commons_node_3.TypeCode.String);
        this.withRequiredProperty('img', pip_services3_commons_node_3.TypeCode.String);
        this.withRequiredProperty('text_color', pip_services3_commons_node_3.TypeCode.String);
        this.withRequiredProperty('text_size', pip_services3_commons_node_3.TypeCode.Integer);
        this.withRequiredProperty('background_color', pip_services3_commons_node_3.TypeCode.String);
        this.withRequiredProperty('size', pip_services3_commons_node_3.TypeCode.String);
        this.withRequiredProperty('critical', pip_services3_commons_node_3.TypeCode.Boolean);
        this.withRequiredProperty('deleted', pip_services3_commons_node_3.TypeCode.Boolean);
        this.withRequiredProperty('disabled', pip_services3_commons_node_3.TypeCode.Boolean);
        this.withOptionalProperty('create_time', pip_services3_commons_node_3.TypeCode.DateTime);
        this.withOptionalProperty('end_time', pip_services3_commons_node_3.TypeCode.DateTime);
    }
}
exports.AdV1Schema = AdV1Schema;
//# sourceMappingURL=AdV1Schema.js.map