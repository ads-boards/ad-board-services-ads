export declare class AdSizeV1 {
    static Small: string;
    static Medium: string;
    static Large: string;
}
