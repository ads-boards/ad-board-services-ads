"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdsServiceFactory = void 0;
const pip_services3_components_node_1 = require("pip-services3-components-node");
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const AdsMongoDbPersistence_1 = require("../persistence/AdsMongoDbPersistence");
const AdsFilePersistence_1 = require("../persistence/AdsFilePersistence");
const AdsMemoryPersistence_1 = require("../persistence/AdsMemoryPersistence");
const AdsController_1 = require("../logic/AdsController");
const AdsHttpServiceV1_1 = require("../services/version1/AdsHttpServiceV1");
const AdsCleanProcessor_1 = require("../logic/AdsCleanProcessor");
class AdsServiceFactory extends pip_services3_components_node_1.Factory {
    constructor() {
        super();
        this.registerAsType(AdsServiceFactory.MemoryPersistenceDescriptor, AdsMemoryPersistence_1.AdsMemoryPersistence);
        this.registerAsType(AdsServiceFactory.FilePersistenceDescriptor, AdsFilePersistence_1.AdsFilePersistence);
        this.registerAsType(AdsServiceFactory.MongoDbPersistenceDescriptor, AdsMongoDbPersistence_1.AdsMongoDbPersistence);
        this.registerAsType(AdsServiceFactory.ControllerDescriptor, AdsController_1.AdsController);
        this.registerAsType(AdsServiceFactory.HttpServiceDescriptor, AdsHttpServiceV1_1.AdsHttpServiceV1);
        this.registerAsType(AdsServiceFactory.CleanProcessorDescriptor, AdsCleanProcessor_1.AdsCleanProcessor);
    }
}
exports.AdsServiceFactory = AdsServiceFactory;
AdsServiceFactory.Descriptor = new pip_services3_commons_node_1.Descriptor("ad-board-ads", "factory", "default", "default", "1.0");
AdsServiceFactory.MemoryPersistenceDescriptor = new pip_services3_commons_node_1.Descriptor("ad-board-ads", "persistence", "memory", "*", "1.0");
AdsServiceFactory.FilePersistenceDescriptor = new pip_services3_commons_node_1.Descriptor("ad-board-ads", "persistence", "file", "*", "1.0");
AdsServiceFactory.MongoDbPersistenceDescriptor = new pip_services3_commons_node_1.Descriptor("ad-board-ads", "persistence", "mongodb", "*", "1.0");
AdsServiceFactory.ControllerDescriptor = new pip_services3_commons_node_1.Descriptor("ad-board-ads", "controller", "default", "*", "1.0");
AdsServiceFactory.HttpServiceDescriptor = new pip_services3_commons_node_1.Descriptor("ad-board-ads", "service", "http", "*", "1.0");
AdsServiceFactory.CleanProcessorDescriptor = new pip_services3_commons_node_1.Descriptor('ad-board-ads', 'processor', 'clean', '*', '1.0');
//# sourceMappingURL=AdsServiceFactory.js.map