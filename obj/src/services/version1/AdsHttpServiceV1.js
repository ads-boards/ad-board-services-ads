"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdsHttpServiceV1 = void 0;
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_rpc_node_1 = require("pip-services3-rpc-node");
class AdsHttpServiceV1 extends pip_services3_rpc_node_1.CommandableHttpService {
    constructor() {
        super('v1/ads');
        this._dependencyResolver.put('controller', new pip_services3_commons_node_1.Descriptor('ad-board-ads', 'controller', 'default', '*', '1.0'));
    }
}
exports.AdsHttpServiceV1 = AdsHttpServiceV1;
//# sourceMappingURL=AdsHttpServiceV1.js.map