import { DataPage } from 'pip-services3-commons-node';
import { FilterParams } from 'pip-services3-commons-node';
import { PagingParams } from 'pip-services3-commons-node';
import { AdV1 } from '../data/version1/AdV1';
export interface IAdsController {
    getAds(correlationId: string, filter: FilterParams, paging: PagingParams, callback: (err: any, page: DataPage<AdV1>) => void): void;
    getAdById(correlationId: string, ad_id: string, callback: (err: any, ad: AdV1) => void): void;
    createAd(correlationId: string, ad: AdV1, callback: (err: any, ad: AdV1) => void): void;
    updateAd(correlationId: string, ad: AdV1, callback: (err: any, ad: AdV1) => void): void;
    deleteAdById(correlationId: string, ad_id: string, callback: (err: any, ad: AdV1) => void): void;
}
