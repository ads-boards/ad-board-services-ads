"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdsController = void 0;
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_commons_node_2 = require("pip-services3-commons-node");
const AdsCommandSet_1 = require("./AdsCommandSet");
class AdsController {
    constructor() {
        this._dependencyResolver = new pip_services3_commons_node_2.DependencyResolver(AdsController._defaultConfig);
    }
    configure(config) {
        this._dependencyResolver.configure(config);
    }
    setReferences(references) {
        this._dependencyResolver.setReferences(references);
        this._persistence = this._dependencyResolver.getOneRequired('persistence');
    }
    getCommandSet() {
        if (this._commandSet == null)
            this._commandSet = new AdsCommandSet_1.AdsCommandSet(this);
        return this._commandSet;
    }
    getAds(correlationId, filter, paging, callback) {
        this._persistence.getPageByFilter(correlationId, filter, paging, callback);
    }
    getAdById(correlationId, id, callback) {
        this._persistence.getOneById(correlationId, id, callback);
    }
    createAd(correlationId, ad, callback) {
        this._persistence.create(correlationId, ad, callback);
    }
    updateAd(correlationId, ad, callback) {
        this._persistence.update(correlationId, ad, callback);
    }
    deleteAdById(correlationId, id, callback) {
        this._persistence.deleteById(correlationId, id, callback);
    }
}
exports.AdsController = AdsController;
AdsController._defaultConfig = pip_services3_commons_node_1.ConfigParams.fromTuples('dependencies.persistence', 'ad-board-ads:persistence:*:*:1.0');
//# sourceMappingURL=AdsController.js.map