import { ConfigParams } from 'pip-services3-commons-node';
import { IConfigurable } from 'pip-services3-commons-node';
import { IReferences } from 'pip-services3-commons-node';
import { IReferenceable } from 'pip-services3-commons-node';
import { FilterParams } from 'pip-services3-commons-node';
import { PagingParams } from 'pip-services3-commons-node';
import { DataPage } from 'pip-services3-commons-node';
import { ICommandable } from 'pip-services3-commons-node';
import { CommandSet } from 'pip-services3-commons-node';
import { AdV1 } from '../data/version1/AdV1';
import { IAdsController } from './IAdsController';
export declare class AdsController implements IConfigurable, IReferenceable, ICommandable, IAdsController {
    private static _defaultConfig;
    private _dependencyResolver;
    private _persistence;
    private _commandSet;
    configure(config: ConfigParams): void;
    setReferences(references: IReferences): void;
    getCommandSet(): CommandSet;
    getAds(correlationId: string, filter: FilterParams, paging: PagingParams, callback: (err: any, page: DataPage<AdV1>) => void): void;
    getAdById(correlationId: string, id: string, callback: (err: any, ad: AdV1) => void): void;
    createAd(correlationId: string, ad: AdV1, callback: (err: any, ad: AdV1) => void): void;
    updateAd(correlationId: string, ad: AdV1, callback: (err: any, ad: AdV1) => void): void;
    deleteAdById(correlationId: string, id: string, callback: (err: any, ad: AdV1) => void): void;
}
