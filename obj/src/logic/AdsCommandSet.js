"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdsCommandSet = void 0;
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_commons_node_2 = require("pip-services3-commons-node");
const pip_services3_commons_node_3 = require("pip-services3-commons-node");
const pip_services3_commons_node_4 = require("pip-services3-commons-node");
const pip_services3_commons_node_5 = require("pip-services3-commons-node");
const pip_services3_commons_node_6 = require("pip-services3-commons-node");
const pip_services3_commons_node_7 = require("pip-services3-commons-node");
const pip_services3_commons_node_8 = require("pip-services3-commons-node");
const AdV1Schema_1 = require("../data/version1/AdV1Schema");
class AdsCommandSet extends pip_services3_commons_node_1.CommandSet {
    constructor(logic) {
        super();
        this._logic = logic;
        // Register commands to the database
        this.addCommand(this.makeGetAdsCommand());
        this.addCommand(this.makeGetAdByIdCommand());
        this.addCommand(this.makeCreateAdCommand());
        this.addCommand(this.makeUpdateAdCommand());
        this.addCommand(this.makeDeleteAdByIdCommand());
    }
    makeGetAdsCommand() {
        return new pip_services3_commons_node_2.Command("get_ads", new pip_services3_commons_node_5.ObjectSchema(true)
            .withOptionalProperty('filter', new pip_services3_commons_node_7.FilterParamsSchema())
            .withOptionalProperty('paging', new pip_services3_commons_node_8.PagingParamsSchema()), (correlationId, args, callback) => {
            let filter = pip_services3_commons_node_3.FilterParams.fromValue(args.get("filter"));
            let paging = pip_services3_commons_node_4.PagingParams.fromValue(args.get("paging"));
            this._logic.getAds(correlationId, filter, paging, callback);
        });
    }
    makeGetAdByIdCommand() {
        return new pip_services3_commons_node_2.Command("get_ad_by_id", new pip_services3_commons_node_5.ObjectSchema(true)
            .withRequiredProperty('ad_id', pip_services3_commons_node_6.TypeCode.String), (correlationId, args, callback) => {
            let ad_id = args.getAsString("ad_id");
            this._logic.getAdById(correlationId, ad_id, callback);
        });
    }
    makeCreateAdCommand() {
        return new pip_services3_commons_node_2.Command("create_ad", new pip_services3_commons_node_5.ObjectSchema(true)
            .withRequiredProperty('ad', new AdV1Schema_1.AdV1Schema()), (correlationId, args, callback) => {
            let ad = args.get("ad");
            this._logic.createAd(correlationId, ad, callback);
        });
    }
    makeUpdateAdCommand() {
        return new pip_services3_commons_node_2.Command("update_ad", new pip_services3_commons_node_5.ObjectSchema(true)
            .withRequiredProperty('ad', new AdV1Schema_1.AdV1Schema()), (correlationId, args, callback) => {
            let ad = args.get("ad");
            this._logic.updateAd(correlationId, ad, callback);
        });
    }
    makeDeleteAdByIdCommand() {
        return new pip_services3_commons_node_2.Command("delete_ad_by_id", new pip_services3_commons_node_5.ObjectSchema(true)
            .withRequiredProperty('ad_id', pip_services3_commons_node_6.TypeCode.String), (correlationId, args, callback) => {
            let adId = args.getAsNullableString("ad_id");
            this._logic.deleteAdById(correlationId, adId, callback);
        });
    }
}
exports.AdsCommandSet = AdsCommandSet;
//# sourceMappingURL=AdsCommandSet.js.map