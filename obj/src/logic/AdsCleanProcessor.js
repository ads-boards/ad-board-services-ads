"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdsCleanProcessor = void 0;
let async = require('async');
let _ = require('lodash');
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_components_node_1 = require("pip-services3-components-node");
class AdsCleanProcessor {
    constructor() {
        this._logger = new pip_services3_components_node_1.CompositeLogger();
        this._timer = new pip_services3_commons_node_1.FixedRateTimer();
        this._correlationId = "ads.cleaner";
        this._interval = 5 * 60 * 1000; // 5 min;
        this._batchSize = 100;
    }
    configure(config) {
        this._logger.configure(config);
        this._interval = config.getAsIntegerWithDefault("options.interval", this._interval);
    }
    setReferences(references) {
        this._logger.setReferences(references);
        this._persistence = references.getOneRequired(new pip_services3_commons_node_1.Descriptor("ad-board-ads", "persistence", "*", "*", "1.0"));
        this._blobsClient = references.getOneOptional(new pip_services3_commons_node_1.Descriptor('pip-services-blobs', 'client', '*', '*', '1.0'));
    }
    open(correlationId, callback) {
        let locCorrelationId = correlationId || this._correlationId;
        this._timer.setDelay(this._interval);
        this._timer.setInterval(this._interval);
        this._timer.setTask({
            notify: (correlationId, args) => {
                this._cleanProcessing(locCorrelationId);
            }
        });
        this._logger.info(correlationId, "Cleaning processing is enable");
        this._timer.start();
        callback(null);
    }
    close(correlationId, callback) {
        correlationId = correlationId || this._correlationId;
        this._timer.stop();
        this._logger.info(correlationId, "Cleaning processing is disable");
        callback(null);
    }
    isOpen() {
        return this._timer != null && this._timer.isStarted();
    }
    _cleanProcessing(correlationId) {
        correlationId = correlationId || this._correlationId;
        this._logger.info(correlationId, "Start cleaning process for ads");
        async.series([
            (callback) => {
                this._persistence.deleteByFilter(correlationId, pip_services3_commons_node_1.FilterParams.fromTuples('deleted', true), callback);
            },
            (callback) => {
                this._persistence.deleteByFilter(correlationId, pip_services3_commons_node_1.FilterParams.fromTuples('end_time_to', new Date()), callback);
            },
        ], (err) => {
            if (err != null) {
                this._logger.error(correlationId, err, "Failed to clean up ads.");
            }
            this._logger.trace(correlationId, "Ads cleaning ended.");
        });
        this._logger.debug(correlationId, "Finished ads claning processes");
        if (this._blobsClient != null) {
            this._logger.info(correlationId, "Start cleaning process for blobs");
            this._logger.info(correlationId, "Starting publishing process for verified practices");
            var deleted = 0;
            var skip = 0;
            var del = true;
            async.whilst(() => {
                return del;
            }, (callback) => {
                var filter = new pip_services3_commons_node_1.FilterParams();
                var paging = new pip_services3_commons_node_1.PagingParams(skip, this._batchSize, false);
                this._blobsClient.getBlobsByFilter(correlationId, filter, paging, (err, page) => {
                    if (err) {
                        this._logger.error(correlationId, err, "Failed to clear blobs");
                        del = false;
                        callback(err);
                        return;
                    }
                    var counter = 0;
                    async.whilst(() => {
                        return counter != page.data.length;
                    }, (cb) => {
                        let blob = page.data[counter];
                        let deletedBlobId = null;
                        counter++;
                        // skip background image
                        if (blob.id.includes("background")) {
                            cb();
                            return;
                        }
                        async.series([
                            (callback) => {
                                this._persistence.getPageByFilter(this._correlationId, pip_services3_commons_node_1.FilterParams.fromTuples("img", blob.id), null, (err, page) => {
                                    if (err) {
                                        callback(err);
                                        return;
                                    }
                                    if (page == null || page.data.length == 0) {
                                        deletedBlobId = blob.id;
                                    }
                                    else {
                                        deletedBlobId = null;
                                    }
                                    callback();
                                });
                            },
                            (callback) => {
                                if (deletedBlobId != null) {
                                    this._blobsClient.deleteBlobById(correlationId, deletedBlobId, (err) => {
                                        if (err) {
                                            this._logger.error(correlationId, err, "Failed to delete blob " + deletedBlobId);
                                        }
                                        else {
                                            deleted++;
                                            this._logger.debug(correlationId, "Blob deleted, blob.id:", deletedBlobId);
                                        }
                                        callback();
                                    });
                                }
                            }
                        ], (err) => {
                            cb();
                        });
                    }, (err) => {
                        if (page.data.length < this._batchSize)
                            del = false;
                        else
                            skip += page.data.length;
                        callback(err);
                    });
                });
            }, (err) => {
                if (deleted > 0)
                    this._logger.info(correlationId, "Deleted " + deleted + " blobs");
                else
                    this._logger.info(correlationId, "Found no blobs for deleting");
                this._logger.debug(correlationId, "Finished clear blobs processes");
            });
        }
    }
}
exports.AdsCleanProcessor = AdsCleanProcessor;
//# sourceMappingURL=AdsCleanProcessor.js.map