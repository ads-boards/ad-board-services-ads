import { CommandSet } from 'pip-services3-commons-node';
import { IAdsController } from './IAdsController';
export declare class AdsCommandSet extends CommandSet {
    private _logic;
    constructor(logic: IAdsController);
    private makeGetAdsCommand;
    private makeGetAdByIdCommand;
    private makeCreateAdCommand;
    private makeUpdateAdCommand;
    private makeDeleteAdByIdCommand;
}
