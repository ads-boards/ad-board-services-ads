"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AdsMemoryPersistence_1 = require("./AdsMemoryPersistence");
Object.defineProperty(exports, "AdsMemoryPersistence", { enumerable: true, get: function () { return AdsMemoryPersistence_1.AdsMemoryPersistence; } });
var AdsFilePersistence_1 = require("./AdsFilePersistence");
Object.defineProperty(exports, "AdsFilePersistence", { enumerable: true, get: function () { return AdsFilePersistence_1.AdsFilePersistence; } });
var AdsMongoDbPersistence_1 = require("./AdsMongoDbPersistence");
Object.defineProperty(exports, "AdsMongoDbPersistence", { enumerable: true, get: function () { return AdsMongoDbPersistence_1.AdsMongoDbPersistence; } });
//# sourceMappingURL=index.js.map