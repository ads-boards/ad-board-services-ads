"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdsFilePersistence = void 0;
const pip_services3_data_node_1 = require("pip-services3-data-node");
const AdsMemoryPersistence_1 = require("./AdsMemoryPersistence");
class AdsFilePersistence extends AdsMemoryPersistence_1.AdsMemoryPersistence {
    constructor(path) {
        super();
        this._persister = new pip_services3_data_node_1.JsonFilePersister(path);
        this._loader = this._persister;
        this._saver = this._persister;
    }
    configure(config) {
        super.configure(config);
        this._persister.configure(config);
    }
}
exports.AdsFilePersistence = AdsFilePersistence;
//# sourceMappingURL=AdsFilePersistence.js.map