"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdsMongoDbPersistence = void 0;
let _ = require('lodash');
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_mongodb_node_1 = require("pip-services3-mongodb-node");
class AdsMongoDbPersistence extends pip_services3_mongodb_node_1.IdentifiableMongoDbPersistence {
    constructor() {
        super('ads');
    }
    composeFilter(filter) {
        filter = filter || new pip_services3_commons_node_1.FilterParams();
        let criteria = [];
        let search = filter.getAsNullableString('search');
        if (search != null) {
            let searchRegex = new RegExp(search, "i");
            let searchCriteria = [];
            searchCriteria.push({ id: { $regex: searchRegex } });
            searchCriteria.push({ title: { $regex: searchRegex } });
            searchCriteria.push({ text: { $regex: searchRegex } });
            criteria.push({ $or: searchCriteria });
        }
        let id = filter.getAsNullableString('id');
        if (id != null)
            criteria.push({ _id: id });
        let publish_group_ids = filter.getAsObject('publish_group_ids');
        if (_.isString(publish_group_ids))
            publish_group_ids = publish_group_ids.split(',');
        if (_.isArray(publish_group_ids))
            criteria.push({ publish_group_ids: { $elemMatch: { $in: publish_group_ids } } });
        let end_time_to = filter.getAsNullableDateTime('end_time_to');
        if (end_time_to != null)
            criteria.push({
                $and: [
                    { end_time: { $ne: null } },
                    { end_time: { $exists: true } },
                    { end_time: { $lte: end_time_to } }
                ]
            });
        let end_time_from = filter.getAsNullableDateTime('end_time_from');
        if (end_time_from != null)
            criteria.push({
                $or: [{
                        $or: [
                            { end_time: { $eq: null } },
                            { end_time: { $exists: false } }
                        ]
                    },
                    {
                        $and: [
                            { end_time: { $ne: null } },
                            { end_time: { $exists: true } },
                            { end_time: { $gt: end_time_from } }
                        ]
                    }]
            });
        let deleted = filter.getAsNullableBoolean('deleted');
        if (deleted != null)
            criteria.push({ deleted: deleted });
        let disabled = filter.getAsNullableBoolean('disabled');
        if (disabled != null)
            criteria.push({ disabled: disabled });
        let img = filter.getAsNullableString('img');
        if (img != null)
            criteria.push({ img: img });
        return criteria.length > 0 ? { $and: criteria } : null;
    }
    getPageByFilter(correlationId, filter, paging, callback) {
        super.getPageByFilter(correlationId, this.composeFilter(filter), paging, null, null, callback);
    }
    deleteByFilter(correlationId, filter, callback) {
        super.deleteByFilter(correlationId, this.composeFilter(filter), callback);
    }
}
exports.AdsMongoDbPersistence = AdsMongoDbPersistence;
//# sourceMappingURL=AdsMongoDbPersistence.js.map