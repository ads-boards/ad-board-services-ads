"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdsMemoryPersistence = void 0;
let _ = require('lodash');
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_data_node_1 = require("pip-services3-data-node");
class AdsMemoryPersistence extends pip_services3_data_node_1.IdentifiableMemoryPersistence {
    constructor() {
        super();
    }
    matchString(value, search) {
        if (value == null && search == null)
            return true;
        if (value == null || search == null)
            return false;
        return value.toLowerCase().indexOf(search) >= 0;
    }
    matchSearch(item, search) {
        search = search.toLowerCase();
        if (this.matchString(item.id, search))
            return true;
        if (this.matchString(item.title, search))
            return true;
        if (this.matchString(item.text, search))
            return true;
        return false;
    }
    composeFilter(filter) {
        filter = filter || new pip_services3_commons_node_1.FilterParams();
        let search = filter.getAsNullableString('search');
        let id = filter.getAsNullableString('id');
        let end_time_to = filter.getAsNullableDateTime('end_time_to');
        let end_time_from = filter.getAsNullableDateTime('end_time_from');
        let deleted = filter.getAsNullableBoolean('deleted');
        let disabled = filter.getAsNullableBoolean('disabled');
        let publish_group_ids = filter.getAsObject('publish_group_ids');
        let img = filter.getAsNullableString('img');
        // Process group_ids filter
        if (_.isString(publish_group_ids))
            publish_group_ids = publish_group_ids.split(',');
        if (!_.isArray(publish_group_ids))
            publish_group_ids = null;
        return (item) => {
            if (search && !this.matchSearch(item, search))
                return false;
            if (id && item.id != id)
                return false;
            if (publish_group_ids && !item.publish_group_ids.some(ai => publish_group_ids.includes(ai)))
                return false;
            if (end_time_to != null && (item.end_time == null || (item.end_time != null && item.end_time.getTime() > end_time_to.getTime())))
                return false;
            if (end_time_from != null && (item.end_time != null && item.end_time.getTime() <= end_time_from.getTime()))
                return false;
            if (deleted && item.deleted != deleted)
                return false;
            if (disabled && item.disabled != disabled)
                return false;
            if (img && item.img != img)
                return false;
            return true;
        };
    }
    getPageByFilter(correlationId, filter, paging, callback) {
        super.getPageByFilter(correlationId, this.composeFilter(filter), paging, null, null, callback);
    }
    deleteByFilter(correlationId, filter, callback) {
        super.deleteByFilter(correlationId, this.composeFilter(filter), callback);
    }
}
exports.AdsMemoryPersistence = AdsMemoryPersistence;
//# sourceMappingURL=AdsMemoryPersistence.js.map