import { ConfigParams } from 'pip-services3-commons-node';
import { JsonFilePersister } from 'pip-services3-data-node';
import { AdsMemoryPersistence } from './AdsMemoryPersistence';
import { AdV1 } from '../data/version1/AdV1';
export declare class AdsFilePersistence extends AdsMemoryPersistence {
    protected _persister: JsonFilePersister<AdV1>;
    constructor(path?: string);
    configure(config: ConfigParams): void;
}
