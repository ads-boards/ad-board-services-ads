"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
let _ = require('lodash');
let async = require('async');
let restify = require('restify');
let assert = require('chai').assert;
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_commons_node_2 = require("pip-services3-commons-node");
const pip_services3_commons_node_3 = require("pip-services3-commons-node");
const AdsMemoryPersistence_1 = require("../../../src/persistence/AdsMemoryPersistence");
const AdsController_1 = require("../../../src/logic/AdsController");
const AdsHttpServiceV1_1 = require("../../../src/services/version1/AdsHttpServiceV1");
const AdSizeV1_1 = require("../../../src/data/version1/AdSizeV1");
let httpConfig = pip_services3_commons_node_1.ConfigParams.fromTuples("connection.protocol", "http", "connection.host", "localhost", "connection.port", 3000);
let AD1 = {
    id: '1',
    publish_group_ids: ['1', '2'],
    title: 'Title 1',
    text: 'Ad text 1',
    img: '',
    text_color: 'black',
    text_size: 14,
    background_color: 'white',
    critical: false,
    deleted: false,
    disabled: false,
    size: AdSizeV1_1.AdSizeV1.Medium,
    create_time: new Date(),
    end_time: new Date()
};
let AD2 = {
    id: '2',
    publish_group_ids: ['1', '3'],
    title: 'Title 2',
    text: 'Ad text 2',
    img: '',
    text_color: 'black',
    text_size: 14,
    background_color: 'white',
    critical: false,
    deleted: false,
    disabled: false,
    size: AdSizeV1_1.AdSizeV1.Medium,
    create_time: new Date(),
    end_time: new Date()
};
suite('AdsHttpServiceV1', () => {
    let service;
    let rest;
    suiteSetup((done) => {
        let persistence = new AdsMemoryPersistence_1.AdsMemoryPersistence();
        let controller = new AdsController_1.AdsController();
        service = new AdsHttpServiceV1_1.AdsHttpServiceV1();
        service.configure(httpConfig);
        let references = pip_services3_commons_node_3.References.fromTuples(new pip_services3_commons_node_2.Descriptor('ad-board-ads', 'persistence', 'memory', 'default', '1.0'), persistence, new pip_services3_commons_node_2.Descriptor('ad-board-ads', 'controller', 'default', 'default', '1.0'), controller, new pip_services3_commons_node_2.Descriptor('ad-board-ads', 'service', 'http', 'default', '1.0'), service);
        controller.setReferences(references);
        service.setReferences(references);
        service.open(null, done);
    });
    suiteTeardown((done) => {
        service.close(null, done);
    });
    setup(() => {
        let url = 'http://localhost:3000';
        rest = restify.createJsonClient({ url: url, version: '*' });
    });
    test('CRUD Operations', (done) => {
        let ad1, ad2;
        async.series([
            // Create one ad
            (callback) => {
                rest.post('/v1/ads/create_ad', {
                    ad: AD1
                }, (err, req, res, ad) => {
                    assert.isNull(err);
                    assert.isObject(ad);
                    assert.equal(ad.id, AD1.id);
                    assert.equal(ad.title, AD1.title);
                    assert.equal(ad.text, AD1.text);
                    ad1 = ad;
                    callback();
                });
            },
            // Create another ad
            (callback) => {
                rest.post('/v1/ads/create_ad', {
                    ad: AD2
                }, (err, req, res, ad) => {
                    assert.isNull(err);
                    assert.isObject(ad);
                    assert.equal(ad.id, AD2.id);
                    assert.equal(ad.title, AD2.title);
                    assert.equal(ad.text, AD2.text);
                    ad2 = ad;
                    callback();
                });
            },
            // Get all ads
            (callback) => {
                rest.post('/v1/ads/get_ads', {}, (err, req, res, page) => {
                    assert.isNull(err);
                    assert.isObject(page);
                    assert.lengthOf(page.data, 2);
                    callback();
                });
            },
            // Update the ad
            (callback) => {
                ad1.text = 'Updated Name 1';
                rest.post('/v1/ads/update_ad', {
                    ad: ad1
                }, (err, req, res, ad) => {
                    assert.isNull(err);
                    assert.isObject(ad);
                    assert.equal(ad.text, 'Updated Name 1');
                    assert.equal(ad.id, AD1.id);
                    ad1 = ad;
                    callback();
                });
            },
            // Delete ad
            (callback) => {
                rest.post('/v1/ads/delete_ad_by_id', {
                    ad_id: ad1.id
                }, (err, req, res, result) => {
                    assert.isNull(err);
                    //assert.isNull(result);
                    callback();
                });
            },
            // Try to get delete ad
            (callback) => {
                rest.post('/v1/ads/get_ad_by_id', {
                    ad_id: ad1.id
                }, (err, req, res, result) => {
                    assert.isNull(err);
                    //assert.isNull(result);
                    callback();
                });
            }
        ], done);
    });
});
//# sourceMappingURL=AdsHttpServiceV1.test.js.map