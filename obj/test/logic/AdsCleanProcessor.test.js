"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
let async = require('async');
let assert = require('chai').assert;
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_commons_node_2 = require("pip-services3-commons-node");
const pip_services3_commons_node_3 = require("pip-services3-commons-node");
const pip_services3_commons_node_4 = require("pip-services3-commons-node");
const pip_services3_commons_node_5 = require("pip-services3-commons-node");
const src_1 = require("../../src");
const AdsCleanProcessor_1 = require("../../src/logic/AdsCleanProcessor");
let now = new Date();
let AD1 = {
    id: '1',
    publish_group_ids: ['1', '2'],
    title: 'Title 1',
    text: 'Ad text 1',
    img: '',
    text_color: 'black',
    text_size: 14,
    background_color: 'white',
    critical: false,
    deleted: false,
    disabled: false,
    size: src_1.AdSizeV1.Medium,
    create_time: now,
    end_time: new Date(now.getTime() + 5000)
};
let AD2 = {
    id: '2',
    publish_group_ids: ['1', '3'],
    title: 'Title 2',
    text: 'Ad text 2',
    img: '',
    text_color: 'black',
    text_size: 14,
    background_color: 'white',
    critical: false,
    deleted: false,
    disabled: false,
    size: src_1.AdSizeV1.Medium,
    create_time: now,
    end_time: new Date(now.getTime() + 3000)
};
let AD3 = {
    id: '3',
    publish_group_ids: ['2', '3'],
    title: 'Title 3',
    text: 'Ad text 3',
    img: '',
    text_color: 'black',
    text_size: 14,
    background_color: 'white',
    critical: false,
    deleted: true,
    disabled: false,
    size: src_1.AdSizeV1.Medium,
    create_time: now,
};
suite('AdsCleanProcessor', () => {
    let persistence;
    let cleanProcessor;
    setup((done) => {
        persistence = new src_1.AdsMemoryPersistence();
        persistence.configure(new pip_services3_commons_node_1.ConfigParams());
        cleanProcessor = new AdsCleanProcessor_1.AdsCleanProcessor();
        cleanProcessor.configure(pip_services3_commons_node_1.ConfigParams.fromTuples('options.interval', '500'));
        let references = pip_services3_commons_node_3.References.fromTuples(new pip_services3_commons_node_2.Descriptor('ad-board-ads', 'persistence', 'memory', 'default', '1.0'), persistence);
        cleanProcessor.setReferences(references);
        persistence.open(null, done);
    });
    teardown((done) => {
        persistence.close(null, done);
    });
    test('Verification processor', (done) => {
        async.series([
            // Create one ad
            (callback) => {
                persistence.create(null, AD1, (err, ad) => {
                    assert.isNull(err);
                    assert.isObject(ad);
                    assert.equal(ad.id, AD1.id);
                    assert.equal(ad.title, AD1.title);
                    assert.equal(ad.text, AD1.text);
                    callback();
                });
            },
            // Create another ad
            (callback) => {
                persistence.create(null, AD2, (err, ad) => {
                    assert.isNull(err);
                    assert.isObject(ad);
                    assert.equal(ad.id, AD2.id);
                    assert.equal(ad.title, AD2.title);
                    assert.equal(ad.text, AD2.text);
                    callback();
                });
            },
            // Create yet another ad
            (callback) => {
                persistence.create(null, AD3, (err, ad) => {
                    assert.isNull(err);
                    assert.isObject(ad);
                    assert.equal(ad.id, AD3.id);
                    assert.equal(ad.title, AD3.title);
                    assert.equal(ad.text, AD3.text);
                    callback();
                });
            },
            // Get all ads
            (callback) => {
                persistence.getPageByFilter(null, new pip_services3_commons_node_4.FilterParams(), new pip_services3_commons_node_5.PagingParams(), (err, page) => {
                    assert.isNull(err);
                    assert.isObject(page);
                    assert.lengthOf(page.data, 3);
                    callback();
                });
            },
            // Run proccessing
            (callback) => {
                cleanProcessor.open(null, (err) => {
                    assert.isNull(err);
                    callback();
                });
            },
            (callback) => {
                setTimeout(() => {
                    callback();
                }, 3100);
            },
            // Get all ads
            (callback) => {
                persistence.getPageByFilter(null, new pip_services3_commons_node_4.FilterParams(), new pip_services3_commons_node_5.PagingParams(), (err, page) => {
                    assert.isNull(err);
                    assert.isObject(page);
                    assert.lengthOf(page.data, 1);
                    assert.equal(page.data[0].id, AD1.id);
                    callback();
                });
            },
            (callback) => {
                cleanProcessor.close(null, (err) => {
                    assert.isNull(err);
                    callback();
                });
            }
        ], done);
    });
});
//# sourceMappingURL=AdsCleanProcessor.test.js.map