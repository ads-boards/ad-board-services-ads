"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const AdsFilePersistence_1 = require("../../src/persistence/AdsFilePersistence");
const AdsPersistenceFixture_1 = require("./AdsPersistenceFixture");
suite('AdsFilePersistence', () => {
    let persistence;
    let fixture;
    setup((done) => {
        persistence = new AdsFilePersistence_1.AdsFilePersistence('./data/ads.test.json');
        fixture = new AdsPersistenceFixture_1.AdsPersistenceFixture(persistence);
        persistence.open(null, (err) => {
            persistence.clear(null, done);
        });
    });
    teardown((done) => {
        persistence.close(null, done);
    });
    test('CRUD Operations', (done) => {
        fixture.testCrudOperations(done);
    });
    test('Get with Filters', (done) => {
        fixture.testGetWithFilter(done);
    });
});
//# sourceMappingURL=AdsFilePersistence.test.js.map