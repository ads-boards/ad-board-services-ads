"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const AdsMemoryPersistence_1 = require("../../src/persistence/AdsMemoryPersistence");
const AdsPersistenceFixture_1 = require("./AdsPersistenceFixture");
suite('AdsMemoryPersistence', () => {
    let persistence;
    let fixture;
    setup((done) => {
        persistence = new AdsMemoryPersistence_1.AdsMemoryPersistence();
        persistence.configure(new pip_services3_commons_node_1.ConfigParams());
        fixture = new AdsPersistenceFixture_1.AdsPersistenceFixture(persistence);
        persistence.open(null, done);
    });
    teardown((done) => {
        persistence.close(null, done);
    });
    test('CRUD Operations', (done) => {
        fixture.testCrudOperations(done);
    });
    test('Get with Filters', (done) => {
        fixture.testGetWithFilter(done);
    });
});
//# sourceMappingURL=AdsMemoryPersistence.test.js.map