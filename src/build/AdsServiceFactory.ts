import { Factory } from 'pip-services3-components-node';
import { Descriptor } from 'pip-services3-commons-node';

import { AdsMongoDbPersistence } from '../persistence/AdsMongoDbPersistence';
import { AdsFilePersistence } from '../persistence/AdsFilePersistence';
import { AdsMemoryPersistence } from '../persistence/AdsMemoryPersistence';
import { AdsController } from '../logic/AdsController';
import { AdsHttpServiceV1 } from '../services/version1/AdsHttpServiceV1';
import { AdsCleanProcessor } from '../logic/AdsCleanProcessor';


export class AdsServiceFactory extends Factory {
	public static Descriptor = new Descriptor("ad-board-ads", "factory", "default", "default", "1.0");
	public static MemoryPersistenceDescriptor = new Descriptor("ad-board-ads", "persistence", "memory", "*", "1.0");
	public static FilePersistenceDescriptor = new Descriptor("ad-board-ads", "persistence", "file", "*", "1.0");
	public static MongoDbPersistenceDescriptor = new Descriptor("ad-board-ads", "persistence", "mongodb", "*", "1.0");
	public static ControllerDescriptor = new Descriptor("ad-board-ads", "controller", "default", "*", "1.0");
	public static HttpServiceDescriptor = new Descriptor("ad-board-ads", "service", "http", "*", "1.0");
	public static CleanProcessorDescriptor = new Descriptor('ad-board-ads', 'processor', 'clean', '*', '1.0');
	
	constructor() {
		super();
		this.registerAsType(AdsServiceFactory.MemoryPersistenceDescriptor, AdsMemoryPersistence);
		this.registerAsType(AdsServiceFactory.FilePersistenceDescriptor, AdsFilePersistence);
		this.registerAsType(AdsServiceFactory.MongoDbPersistenceDescriptor, AdsMongoDbPersistence);
		this.registerAsType(AdsServiceFactory.ControllerDescriptor, AdsController);
		this.registerAsType(AdsServiceFactory.HttpServiceDescriptor, AdsHttpServiceV1);
		this.registerAsType(AdsServiceFactory.CleanProcessorDescriptor, AdsCleanProcessor);
		
	}
	
}
