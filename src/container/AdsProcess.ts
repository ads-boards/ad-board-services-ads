
import { ProcessContainer } from 'pip-services3-container-node';

import { AdsServiceFactory } from '../build/AdsServiceFactory';
import { DefaultRpcFactory } from 'pip-services3-rpc-node';
import { DefaultGrpcFactory } from 'pip-services3-grpc-node';

export class AdsProcess extends ProcessContainer {

    public constructor() {
        super("ads-library", "Ads microservice");
        this._factories.add(new AdsServiceFactory);
        this._factories.add(new DefaultRpcFactory);
        this._factories.add(new DefaultGrpcFactory);
    }

}
