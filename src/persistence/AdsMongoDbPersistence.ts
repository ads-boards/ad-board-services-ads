let _ = require('lodash');

import { FilterParams } from 'pip-services3-commons-node';
import { PagingParams } from 'pip-services3-commons-node';
import { DataPage } from 'pip-services3-commons-node';
import { IdentifiableMongoDbPersistence } from 'pip-services3-mongodb-node';

import { AdV1 } from '../data/version1/AdV1';
import { IAdsPersistence } from './IAdsPersistence';

export class AdsMongoDbPersistence
    extends IdentifiableMongoDbPersistence<AdV1, string>
    implements IAdsPersistence {

    constructor() {
        super('ads');
    }

    private composeFilter(filter: any) {
        filter = filter || new FilterParams();

        let criteria = [];

        let search = filter.getAsNullableString('search');
        if (search != null) {
            let searchRegex = new RegExp(search, "i");
            let searchCriteria = [];
            searchCriteria.push({ id: { $regex: searchRegex } });
            searchCriteria.push({ title: { $regex: searchRegex } });
            searchCriteria.push({ text: { $regex: searchRegex } });
            criteria.push({ $or: searchCriteria });
        }

        let id = filter.getAsNullableString('id');
        if (id != null)
            criteria.push({ _id: id });

        let publish_group_ids = filter.getAsObject('publish_group_ids');
        if (_.isString(publish_group_ids))
            publish_group_ids = publish_group_ids.split(',');
        if (_.isArray(publish_group_ids))
            criteria.push({ publish_group_ids: { $elemMatch: { $in: publish_group_ids } } });

        let end_time_to = filter.getAsNullableDateTime('end_time_to');
        if (end_time_to != null)
            criteria.push({
                $and: [
                    { end_time: { $ne: null } },
                    { end_time: { $exists: true } },
                    { end_time: { $lte: end_time_to } }
                ]
            });

        let end_time_from = filter.getAsNullableDateTime('end_time_from');
        if (end_time_from != null)
            criteria.push(
                {
                    $or: [{
                        $or: [
                            { end_time: { $eq: null } },
                            { end_time: { $exists: false } }

                        ]
                    },
                    {
                        $and: [
                            { end_time: { $ne: null } },
                            { end_time: { $exists: true } },
                            { end_time: { $gt: end_time_from } }
                        ]
                    }]
                });
        let deleted = filter.getAsNullableBoolean('deleted');
        if (deleted != null)
            criteria.push({ deleted: deleted });

        let disabled = filter.getAsNullableBoolean('disabled');
        if (disabled != null)
            criteria.push({ disabled: disabled });

        let img = filter.getAsNullableString('img');
            if (img != null)
                criteria.push({ img: img });


        return criteria.length > 0 ? { $and: criteria } : null;
    }

    public getPageByFilter(correlationId: string, filter: FilterParams, paging: PagingParams,
        callback: (err: any, page: DataPage<AdV1>) => void): void {
        super.getPageByFilter(correlationId, this.composeFilter(filter), paging, null, null, callback);
    }

    public deleteByFilter(correlationId: string, filter: FilterParams, callback: (err: any) => void): void {
        super.deleteByFilter(correlationId, this.composeFilter(filter), callback);
    }

}
