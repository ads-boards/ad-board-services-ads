import { FilterParams } from 'pip-services3-commons-node';
import { PagingParams } from 'pip-services3-commons-node';
import { DataPage } from 'pip-services3-commons-node';
import { IGetter } from 'pip-services3-data-node';
import { IWriter } from 'pip-services3-data-node';

import { AdV1 } from '../data/version1/AdV1';

export interface IAdsPersistence extends IGetter<AdV1, string>, IWriter<AdV1, string> {
    getPageByFilter(correlationId: string, filter: FilterParams, paging: PagingParams,
        callback: (err: any, page: DataPage<AdV1>) => void): void;

    getOneById(correlationId: string, id: string,
        callback: (err: any, item: AdV1) => void): void;

    create(correlationId: string, item: AdV1,
        callback: (err: any, item: AdV1) => void): void;

    update(correlationId: string, item: AdV1,
        callback: (err: any, item: AdV1) => void): void;

    deleteById(correlationId: string, id: string,
        callback: (err: any, item: AdV1) => void): void;

    deleteByFilter(correlationId: string, filter: FilterParams,
        callback: (err: any) => void): void
}
