let _ = require('lodash');

import { FilterParams } from 'pip-services3-commons-node';
import { PagingParams } from 'pip-services3-commons-node';
import { DataPage } from 'pip-services3-commons-node';
import { IdentifiableMemoryPersistence } from 'pip-services3-data-node';

import { AdV1 } from '../data/version1/AdV1';
import { IAdsPersistence } from './IAdsPersistence';

export class AdsMemoryPersistence
    extends IdentifiableMemoryPersistence<AdV1, string>
    implements IAdsPersistence {

    constructor() {
        super();
    }

    private matchString(value: string, search: string): boolean {
        if (value == null && search == null)
            return true;
        if (value == null || search == null)
            return false;
        return value.toLowerCase().indexOf(search) >= 0;
    }

    private matchSearch(item: AdV1, search: string): boolean {
        search = search.toLowerCase();
        if (this.matchString(item.id, search))
            return true;
        if (this.matchString(item.title, search))
            return true;
        if (this.matchString(item.text, search))
            return true;
        return false;
    }

    private composeFilter(filter: FilterParams): any {
        filter = filter || new FilterParams();

        let search = filter.getAsNullableString('search');
        let id = filter.getAsNullableString('id');
        let end_time_to = filter.getAsNullableDateTime('end_time_to');
        let end_time_from = filter.getAsNullableDateTime('end_time_from');
        let deleted = filter.getAsNullableBoolean('deleted');
        let disabled = filter.getAsNullableBoolean('disabled');
        let publish_group_ids = filter.getAsObject('publish_group_ids');
        let img = filter.getAsNullableString('img');
        // Process group_ids filter
        if (_.isString(publish_group_ids))
            publish_group_ids = publish_group_ids.split(',');
        if (!_.isArray(publish_group_ids))
            publish_group_ids = null;

        return (item) => {
            if (search && !this.matchSearch(item, search))
                return false;
            if (id && item.id != id)
                return false;
            if (publish_group_ids && !item.publish_group_ids.some(ai => publish_group_ids.includes(ai)))
                return false;
            if (end_time_to != null && (item.end_time == null || (item.end_time != null && item.end_time.getTime() > end_time_to.getTime())))
                return false;
            if (end_time_from != null && (item.end_time != null && item.end_time.getTime() <= end_time_from.getTime()))
                return false;
            if (deleted && item.deleted != deleted)
                return false;
            if (disabled && item.disabled != disabled)
                return false;
            if (img && item.img != img)
                return false;

            return true;
        };
    }

    public getPageByFilter(correlationId: string, filter: FilterParams, paging: PagingParams,
        callback: (err: any, page: DataPage<AdV1>) => void): void {
        super.getPageByFilter(correlationId, this.composeFilter(filter), paging, null, null, callback);
    }

    public deleteByFilter(correlationId: string, filter: FilterParams, callback: (err: any) => void): void {
        super.deleteByFilter(correlationId, this.composeFilter(filter), callback);
    }

}
