let async = require('async');
let _ = require('lodash');

import { IConfigurable, IReferenceable, IOpenable, ConfigParams, IReferences, FixedRateTimer, Descriptor, FilterParams, PagingParams } from "pip-services3-commons-node";
import { CompositeLogger } from "pip-services3-components-node";
import { Parameters } from "pip-services3-commons-node";
import { IAdsPersistence } from "..";
import { IBlobsClientV1 } from "pip-clients-blobs-node/obj/src/version1/IBlobsClientV1";
import { BlobInfoV1 } from "pip-clients-blobs-node/obj/src/version1/BlobInfoV1";

export class AdsCleanProcessor implements IConfigurable, IReferenceable, IOpenable {
    private _logger: CompositeLogger = new CompositeLogger();
    private _timer: FixedRateTimer = new FixedRateTimer();

    private _persistence: IAdsPersistence;
    private _blobsClient: IBlobsClientV1;
    private _correlationId: string = "ads.cleaner";
    private _interval: number = 5 * 60 * 1000; // 5 min;
    private _batchSize: number = 100;

    constructor() {
    }

    public configure(config: ConfigParams): void {
        this._logger.configure(config);
        this._interval = config.getAsIntegerWithDefault("options.interval", this._interval);
    }

    public setReferences(references: IReferences): void {
        this._logger.setReferences(references);
        this._persistence = references.getOneRequired<IAdsPersistence>(new Descriptor("ad-board-ads", "persistence", "*", "*", "1.0"));
        this._blobsClient = references.getOneOptional<IBlobsClientV1>(new Descriptor('pip-services-blobs', 'client', '*', '*', '1.0'));
    }

    public open(correlationId: string, callback: (err: any) => void) {
        let locCorrelationId = correlationId || this._correlationId;
        this._timer.setDelay(this._interval);
        this._timer.setInterval(this._interval);
        this._timer.setTask({
            notify: (correlationId: string, args: Parameters) => {
                this._cleanProcessing(locCorrelationId);
            }
        });
        this._logger.info(correlationId, "Cleaning processing is enable");
        this._timer.start();
        callback(null);
    }

    public close(correlationId: string, callback: (err: any) => any) {
        correlationId = correlationId || this._correlationId;
        this._timer.stop();
        this._logger.info(correlationId, "Cleaning processing is disable");
        callback(null);
    }

    public isOpen(): boolean {
        return this._timer != null && this._timer.isStarted();
    }

    private _cleanProcessing(correlationId: string): void {
        correlationId = correlationId || this._correlationId;

        this._logger.info(correlationId, "Start cleaning process for ads");
        async.series([
            (callback) => {
                this._persistence.deleteByFilter(
                    correlationId,
                    FilterParams.fromTuples(
                        'deleted', true
                    ),
                    callback
                );
            },
            (callback) => {
                this._persistence.deleteByFilter(
                    correlationId,
                    FilterParams.fromTuples(
                        'end_time_to', new Date()
                    ),
                    callback
                );
            },
        ], (err) => {
            if (err != null) {
                this._logger.error(correlationId, err, "Failed to clean up ads.");
            }
            this._logger.trace(correlationId, "Ads cleaning ended.");
        });

        this._logger.debug(correlationId, "Finished ads claning processes");

        if (this._blobsClient != null) {
            this._logger.info(correlationId, "Start cleaning process for blobs");

            this._logger.info(correlationId, "Starting publishing process for verified practices");

            var deleted = 0;
            var skip = 0;
            var del: boolean = true;

            async.whilst(() => {
                return del;
            },
                (callback) => {
                    var filter = new FilterParams();
                    var paging = new PagingParams(skip, this._batchSize, false);

                    this._blobsClient.getBlobsByFilter(correlationId, filter, paging, (err, page) => {
                        if (err) {
                            this._logger.error(correlationId, err, "Failed to clear blobs");
                            del = false;
                            callback(err);
                            return;
                        }
                        var counter = 0
                        async.whilst(() => {
                            return counter != page.data.length
                        },
                            (cb) => {
                                let blob: BlobInfoV1 = page.data[counter];
                                let deletedBlobId: string = null;
                                counter++;
                                // skip background image
                                if (blob.id.includes("background")) {
                                    cb();
                                    return;
                                }
                                async.series([
                                    (callback) => {
                                        this._persistence.getPageByFilter(this._correlationId, FilterParams.fromTuples("img", blob.id), null, (err, page) => {

                                            if (err) {
                                                callback(err);
                                                return;
                                            }
                                            if (page == null || page.data.length == 0) {
                                                deletedBlobId = blob.id
                                            } else {
                                                deletedBlobId = null
                                            }
                                            callback();
                                        });
                                    },

                                    (callback) => {
                                        if (deletedBlobId != null) {
                                            this._blobsClient.deleteBlobById(correlationId, deletedBlobId, (err) => {
                                                if (err) {
                                                    this._logger.error(correlationId, err, "Failed to delete blob " + deletedBlobId);
                                                } else {
                                                    deleted++;
                                                    this._logger.debug(correlationId, "Blob deleted, blob.id:", deletedBlobId);
                                                }
                                                callback();
                                            });
                                        }
                                    }
                                ],
                                    (err) => {
                                        cb();
                                    }
                                );
                            },
                            (err) => {
                                if (page.data.length < this._batchSize)
                                    del = false;
                                else
                                    skip += page.data.length;
                                callback(err);
                            });
                    })
                }, (err) => {
                    if (deleted > 0)
                        this._logger.info(correlationId, "Deleted " + deleted + " blobs");
                    else
                        this._logger.info(correlationId, "Found no blobs for deleting");
                    this._logger.debug(correlationId, "Finished clear blobs processes");
                }
            )
        }
    }
}

