import { ConfigParams } from 'pip-services3-commons-node';
import { IConfigurable } from 'pip-services3-commons-node';
import { IReferences } from 'pip-services3-commons-node';
import { Descriptor } from 'pip-services3-commons-node';
import { IReferenceable } from 'pip-services3-commons-node';
import { DependencyResolver } from 'pip-services3-commons-node';
import { FilterParams } from 'pip-services3-commons-node';
import { PagingParams } from 'pip-services3-commons-node';
import { DataPage } from 'pip-services3-commons-node';
import { ICommandable } from 'pip-services3-commons-node';
import { CommandSet } from 'pip-services3-commons-node';
import { TagsProcessor } from 'pip-services3-commons-node';

import { AdV1 } from '../data/version1/AdV1';
import { IAdsPersistence } from '../persistence/IAdsPersistence';
import { IAdsController } from './IAdsController';
import { AdsCommandSet } from './AdsCommandSet';

export class AdsController implements  IConfigurable, IReferenceable, ICommandable, IAdsController {
    private static _defaultConfig: ConfigParams = ConfigParams.fromTuples(
        'dependencies.persistence', 'ad-board-ads:persistence:*:*:1.0'
    );

    private _dependencyResolver: DependencyResolver = new DependencyResolver(AdsController._defaultConfig);
    private _persistence: IAdsPersistence;
    private _commandSet: AdsCommandSet;

    public configure(config: ConfigParams): void {
        this._dependencyResolver.configure(config);
    }

    public setReferences(references: IReferences): void {
        this._dependencyResolver.setReferences(references);
        this._persistence = this._dependencyResolver.getOneRequired<IAdsPersistence>('persistence');
    }

    public getCommandSet(): CommandSet {
        if (this._commandSet == null)
            this._commandSet = new AdsCommandSet(this);
        return this._commandSet;
    }
    
    public getAds(correlationId: string, filter: FilterParams, paging: PagingParams, 
        callback: (err: any, page: DataPage<AdV1>) => void): void {
        this._persistence.getPageByFilter(correlationId, filter, paging, callback);
    }

    public getAdById(correlationId: string, id: string, 
        callback: (err: any, ad: AdV1) => void): void {
        this._persistence.getOneById(correlationId, id, callback);        
    }

    public createAd(correlationId: string, ad: AdV1, 
        callback: (err: any, ad: AdV1) => void): void {
        this._persistence.create(correlationId, ad, callback);
    }

    public updateAd(correlationId: string, ad: AdV1, 
        callback: (err: any, ad: AdV1) => void): void {
        this._persistence.update(correlationId, ad, callback);
    }

    public deleteAdById(correlationId: string, id: string,
        callback: (err: any, ad: AdV1) => void): void {  
        this._persistence.deleteById(correlationId, id, callback);
    }

}
