import { CommandSet } from 'pip-services3-commons-node';
import { ICommand } from 'pip-services3-commons-node';
import { Command } from 'pip-services3-commons-node';
import { Schema } from 'pip-services3-commons-node';
import { Parameters } from 'pip-services3-commons-node';
import { FilterParams } from 'pip-services3-commons-node';
import { PagingParams } from 'pip-services3-commons-node';
import { ObjectSchema } from 'pip-services3-commons-node';
import { TypeCode } from 'pip-services3-commons-node';
import { FilterParamsSchema } from 'pip-services3-commons-node';
import { PagingParamsSchema } from 'pip-services3-commons-node';

import { AdV1Schema } from '../data/version1/AdV1Schema';
import { IAdsController } from './IAdsController';

export class AdsCommandSet extends CommandSet {
    private _logic: IAdsController;

    constructor(logic: IAdsController) {
        super();

        this._logic = logic;

        // Register commands to the database
		this.addCommand(this.makeGetAdsCommand());
		this.addCommand(this.makeGetAdByIdCommand());
		this.addCommand(this.makeCreateAdCommand());
		this.addCommand(this.makeUpdateAdCommand());
		this.addCommand(this.makeDeleteAdByIdCommand());
    }

	private makeGetAdsCommand(): ICommand {
		return new Command(
			"get_ads",
			new ObjectSchema(true)
				.withOptionalProperty('filter', new FilterParamsSchema())
				.withOptionalProperty('paging', new PagingParamsSchema()),
            (correlationId: string, args: Parameters, callback: (err: any, result: any) => void) => {
                let filter = FilterParams.fromValue(args.get("filter"));
                let paging = PagingParams.fromValue(args.get("paging"));
                this._logic.getAds(correlationId, filter, paging, callback);
            }
		);
	}

	private makeGetAdByIdCommand(): ICommand {
		return new Command(
			"get_ad_by_id",
			new ObjectSchema(true)
				.withRequiredProperty('ad_id', TypeCode.String),
            (correlationId: string, args: Parameters, callback: (err: any, result: any) => void) => {
                let ad_id = args.getAsString("ad_id");
                this._logic.getAdById(correlationId, ad_id, callback);
            }
		);
	}

	private makeCreateAdCommand(): ICommand {
		return new Command(
			"create_ad",
			new ObjectSchema(true)
				.withRequiredProperty('ad', new AdV1Schema()),
            (correlationId: string, args: Parameters, callback: (err: any, result: any) => void) => {
                let ad = args.get("ad");
                this._logic.createAd(correlationId, ad, callback);
            }
		);
	}

	private makeUpdateAdCommand(): ICommand {
		return new Command(
			"update_ad",
			new ObjectSchema(true)
				.withRequiredProperty('ad', new AdV1Schema()),
            (correlationId: string, args: Parameters, callback: (err: any, result: any) => void) => {
                let ad = args.get("ad");
                this._logic.updateAd(correlationId, ad, callback);
            }
		);
	}
	
	private makeDeleteAdByIdCommand(): ICommand {
		return new Command(
			"delete_ad_by_id",
			new ObjectSchema(true)
				.withRequiredProperty('ad_id', TypeCode.String),
            (correlationId: string, args: Parameters, callback: (err: any, result: any) => void) => {
                let adId = args.getAsNullableString("ad_id");
                this._logic.deleteAdById(correlationId, adId, callback);
			}
		);
	}

}