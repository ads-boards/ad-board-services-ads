import { ObjectSchema } from 'pip-services3-commons-node';
import { ArraySchema } from 'pip-services3-commons-node';
import { TypeCode } from 'pip-services3-commons-node';

export class AdV1Schema extends ObjectSchema {
    public constructor() {
        super();
        this.withOptionalProperty('id', TypeCode.String);
        this.withRequiredProperty('publish_group_ids', new ArraySchema(TypeCode.String));
        this.withRequiredProperty('title', TypeCode.String);
        this.withRequiredProperty('text', TypeCode.String);
        this.withRequiredProperty('img', TypeCode.String);
        this.withRequiredProperty('text_color', TypeCode.String);
        this.withRequiredProperty('text_size', TypeCode.Integer);
        this.withRequiredProperty('background_color', TypeCode.String);
        this.withRequiredProperty('size', TypeCode.String);
        this.withRequiredProperty('critical', TypeCode.Boolean);
        this.withRequiredProperty('deleted', TypeCode.Boolean);
        this.withRequiredProperty('disabled', TypeCode.Boolean);
        this.withOptionalProperty('create_time', TypeCode.DateTime);
        this.withOptionalProperty('end_time', TypeCode.DateTime);
    }
}
