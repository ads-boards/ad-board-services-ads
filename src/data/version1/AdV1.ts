let _ = require('lodash');

import { IStringIdentifiable } from 'pip-services3-commons-node';

export class AdV1 implements IStringIdentifiable {
    public id: string;
    public publish_group_ids: string[]; // published groups ids
    public title: string; // ad title
    public text: string; // ad text
    public img: string; // image url
    public text_color:string; // ad text color
    public text_size:number; // ad text size
    public background_color: string; // ad card background color

    public critical: boolean; // is critical
    public deleted: boolean;  // is deleted

    public size:string; // Size of ad card on board
    public disabled: boolean; // disabled ad, but not delete

    public create_time?: Date;
    public end_time?: Date;
}