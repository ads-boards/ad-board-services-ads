import { Descriptor } from 'pip-services3-commons-node';
import { CommandableHttpService } from 'pip-services3-rpc-node';

export class AdsHttpServiceV1 extends CommandableHttpService {
    public constructor() {
        super('v1/ads');
        this._dependencyResolver.put('controller', new Descriptor('ad-board-ads', 'controller', 'default', '*', '1.0'));
    }
}