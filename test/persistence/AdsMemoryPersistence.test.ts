import { ConfigParams } from 'pip-services3-commons-node';

import { AdsMemoryPersistence } from '../../src/persistence/AdsMemoryPersistence';
import { AdsPersistenceFixture } from './AdsPersistenceFixture';

suite('AdsMemoryPersistence', ()=> {
    let persistence: AdsMemoryPersistence;
    let fixture: AdsPersistenceFixture;
    
    setup((done) => {
        persistence = new AdsMemoryPersistence();
        persistence.configure(new ConfigParams());
        
        fixture = new AdsPersistenceFixture(persistence);
        
        persistence.open(null, done);
    });
    
    teardown((done) => {
        persistence.close(null, done);
    });
        
    test('CRUD Operations', (done) => {
        fixture.testCrudOperations(done);
    });

    test('Get with Filters', (done) => {
        fixture.testGetWithFilter(done);
    });

});