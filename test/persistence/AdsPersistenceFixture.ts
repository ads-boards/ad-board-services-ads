let _ = require('lodash');
let async = require('async');
let assert = require('chai').assert;

import { FilterParams, MultiString } from 'pip-services3-commons-node';
import { PagingParams } from 'pip-services3-commons-node';
import { AdSizeV1 } from '../../src';

import { AdV1 } from '../../src/data/version1/AdV1';

import { IAdsPersistence } from '../../src/persistence/IAdsPersistence';

let now = new Date();

let AD1: AdV1 = {
     id: '1',
     publish_group_ids: ['1', '2'], 
     title: 'Title 1',
     text: 'Ad text 1',
     img: '', 
     text_color:'black',
     text_size: 14,
     background_color: 'white',
     critical: false, 
     deleted: false,
     disabled: false,
     size:AdSizeV1.Medium,
     create_time: now,
     end_time: new Date(now.getTime() + 3600)
};
let AD2: AdV1 = {
    id: '2',
    publish_group_ids: ['1', '3'], 
    title: 'Title 2',
    text: 'Ad text 2',
    img: '', 
    text_color:'black',
    text_size: 14,
    background_color: 'white',
    critical: false, 
    deleted: false,
    disabled: false,
    size:AdSizeV1.Medium,
    create_time: now,
    end_time: new Date(now.getTime() + 3000)
};
let AD3: AdV1 = {
    id: '3',
     publish_group_ids: ['2', '3'], 
     title: 'Title 3',
     text: 'Ad text 3',
     img: '', 
     text_color:'black',
     text_size: 14,
     background_color: 'white',
     critical: false, 
     deleted: true,
     disabled: false,
     size:AdSizeV1.Medium,
     create_time: now,
     //end_time: new Date(now.getTime() + 36000)
};

export class AdsPersistenceFixture {
    private _persistence: IAdsPersistence;
    
    constructor(persistence) {
        assert.isNotNull(persistence);
        this._persistence = persistence;
    }

    private testCreateAds(done) {
        async.series([
        // Create one ad
            (callback) => {
                this._persistence.create(
                    null,
                    AD1,
                    (err, ad) => {
                        assert.isNull(err);

                        assert.isObject(ad);
                        assert.equal(ad.id, AD1.id);
                        assert.equal(ad.title, AD1.title);
                        assert.equal(ad.text, AD1.text);

                        callback();
                    }
                );
            },
        // Create another ad
            (callback) => {
                this._persistence.create(
                    null,
                    AD2,
                    (err, ad) => {
                        assert.isNull(err);

                        assert.isObject(ad);
                        assert.equal(ad.id, AD2.id);
                        assert.equal(ad.title, AD2.title);
                        assert.equal(ad.text, AD2.text);

                        callback();
                    }
                );
            },
        // Create yet another ad
            (callback) => {
                this._persistence.create(
                    null,
                    AD3,
                    (err, ad) => {
                        assert.isNull(err);

                        assert.isObject(ad);
                        assert.equal(ad.id, AD3.id);
                        assert.equal(ad.title, AD3.title);
                        assert.equal(ad.text, AD3.text);

                        callback();
                    }
                );
            }
        ], done);
    }
                
    testCrudOperations(done) {
        let ad1: AdV1;

        async.series([
        // Create items
            (callback) => {
                this.testCreateAds(callback);
            },
        // Get all ads
            (callback) => {
                this._persistence.getPageByFilter(
                    null,
                    new FilterParams(),
                    new PagingParams(),
                    (err, page) => {
                        assert.isNull(err);

                        assert.isObject(page);
                        assert.lengthOf(page.data, 3);

                        ad1 = page.data[0];

                        callback();
                    }
                );
            },
        // Update the ad
            (callback) => {
                ad1.text =  'Updated Text 1';

                this._persistence.update(
                    null,
                    ad1,
                    (err, ad) => {
                        assert.isNull(err);

                        assert.isObject(ad);
                        assert.equal(ad.text, 'Updated Text 1');
                        assert.equal(ad.id, ad1.id);

                        callback();
                    }
                );
            },
        // Delete ad
            (callback) => {
                this._persistence.deleteById(
                    null,
                    ad1.id,
                    (err) => {
                        assert.isNull(err);

                        callback();
                    }
                );
            },
        // Try to get delete ad
            (callback) => {
                this._persistence.getOneById(
                    null,
                    ad1.id,
                    (err, ad) => {
                        assert.isNull(err);

                        assert.isNull(ad || null);

                        callback();
                    }
                );
            }
        ], done);
    }

    testGetWithFilter(done) {
        async.series([
        // Create ads
            (callback) => {
                this.testCreateAds(callback);
            },
        // Get ads filtered by id
            (callback) => {
                this._persistence.getPageByFilter(
                    null,
                    FilterParams.fromValue({
                        id: '1'
                    }),
                    new PagingParams(),
                    (err, ads) => {
                        assert.isNull(err);

                        assert.isObject(ads);
                        assert.lengthOf(ads.data, 1);

                        callback();
                    }
                );
            },
            // Get ads filtered by publish_group_ids
            (callback) => {
                this._persistence.getPageByFilter(
                    null,
                    FilterParams.fromValue({
                        publish_group_ids: ['1']
                    }),
                    new PagingParams(),
                    (err, ads) => {
                        assert.isNull(err);

                        assert.isObject(ads);
                        assert.lengthOf(ads.data, 2);

                        callback();
                    }
                );
            },
            // Get ads filtered by publish_group_ids
            (callback) => {
                this._persistence.getPageByFilter(
                    null,
                    FilterParams.fromValue({
                        publish_group_ids: ['1','2']
                    }),
                    new PagingParams(),
                    (err, ads) => {
                        assert.isNull(err);

                        assert.isObject(ads);
                        assert.lengthOf(ads.data, 3);

                        callback();
                    }
                );
            },
            // Get ads filtered by delete
            (callback) => {
                this._persistence.getPageByFilter(
                    null,
                    FilterParams.fromValue({
                        deleted: true
                    }),
                    new PagingParams(),
                    (err, ads) => {
                        assert.isNull(err);

                        assert.isObject(ads);
                        assert.lengthOf(ads.data, 1);

                        callback();
                    }
                );
            },
            // Get ads filtered by expired end_time_to 
            (callback) => {
                this._persistence.getPageByFilter(
                    null,
                    FilterParams.fromValue({
                        end_time_to: new Date(now.getTime() + 4000)
                    }),
                    new PagingParams(),
                    (err, ads) => {
                        assert.isNull(err);

                        assert.isObject(ads);
                        assert.lengthOf(ads.data, 2);

                        callback();
                    }
                );
            },
            // Get ads filtered by expired end_time_from
            (callback) => {
                this._persistence.getPageByFilter(
                    null,
                    FilterParams.fromValue({
                        end_time_from: new Date(now.getTime() + 3100)
                    }),
                    new PagingParams(),
                    (err, ads) => {
                        assert.isNull(err);

                        assert.isObject(ads);
                        assert.lengthOf(ads.data, 2);

                        callback();
                    }
                );
            },
        // Get ads filtered by search
            (callback) => {
                this._persistence.getPageByFilter(
                    null,
                    FilterParams.fromValue({
                        search: 'text'
                    }),
                    new PagingParams(),
                    (err, ads) => {
                        assert.isNull(err);

                        assert.isObject(ads);
                        assert.lengthOf(ads.data, 3);

                        callback();
                    }
                );
            }
        ], done);
    }

}
