import { ConfigParams } from 'pip-services3-commons-node';

import { AdsFilePersistence } from '../../src/persistence/AdsFilePersistence';
import { AdsPersistenceFixture } from './AdsPersistenceFixture';

suite('AdsFilePersistence', ()=> {
    let persistence: AdsFilePersistence;
    let fixture: AdsPersistenceFixture;
    
    setup((done) => {
        persistence = new AdsFilePersistence('./data/ads.test.json');

        fixture = new AdsPersistenceFixture(persistence);

        persistence.open(null, (err) => {
            persistence.clear(null, done);
        });
    });
    
    teardown((done) => {
        persistence.close(null, done);
    });
        
    test('CRUD Operations', (done) => {
        fixture.testCrudOperations(done);
    });

    test('Get with Filters', (done) => {
        fixture.testGetWithFilter(done);
    });

});