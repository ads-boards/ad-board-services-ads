let async = require('async');
let assert = require('chai').assert;

import { ConfigParams } from 'pip-services3-commons-node';
import { Descriptor } from 'pip-services3-commons-node';
import { References } from 'pip-services3-commons-node';
import { FilterParams } from 'pip-services3-commons-node';
import { PagingParams } from 'pip-services3-commons-node';
import { AdSizeV1, AdsMemoryPersistence, AdV1 } from '../../src';
import { AdsCleanProcessor } from '../../src/logic/AdsCleanProcessor';




let now = new Date();

let AD1: AdV1 = {
    id: '1',
    publish_group_ids: ['1', '2'],
    title: 'Title 1',
    text: 'Ad text 1',
    img: '',
    text_color: 'black',
    text_size: 14,
    background_color: 'white',
    critical: false,
    deleted: false,
    disabled: false,
    size:AdSizeV1.Medium,
    create_time: now,
    end_time: new Date(now.getTime() + 5000)
};
let AD2: AdV1 = {
    id: '2',
    publish_group_ids: ['1', '3'],
    title: 'Title 2',
    text: 'Ad text 2',
    img: '',
    text_color: 'black',
    text_size: 14,
    background_color: 'white',
    critical: false,
    deleted: false,
    disabled: false,
    size:AdSizeV1.Medium,
    create_time: now,
    end_time: new Date(now.getTime() + 3000)
};
let AD3: AdV1 = {
    id: '3',
    publish_group_ids: ['2', '3'],
    title: 'Title 3',
    text: 'Ad text 3',
    img: '',
    text_color: 'black',
    text_size: 14,
    background_color: 'white',
    critical: false,
    deleted: true,
    disabled: false,
    size:AdSizeV1.Medium,
    create_time: now,
    //end_time: new Date(now.getTime() + 36000)
};

suite('AdsCleanProcessor', () => {

    let persistence: AdsMemoryPersistence;

    let cleanProcessor: AdsCleanProcessor;

    setup((done) => {
        persistence = new AdsMemoryPersistence();
        persistence.configure(new ConfigParams());


        cleanProcessor = new AdsCleanProcessor();
        cleanProcessor.configure(ConfigParams.fromTuples('options.interval', '500'));

        let references = References.fromTuples(
            new Descriptor('ad-board-ads', 'persistence', 'memory', 'default', '1.0'), persistence,
        );

        cleanProcessor.setReferences(references);
        persistence.open(null, done);


    });

    teardown((done) => {
        persistence.close(null, done);
    });

    test('Verification processor', (done) => {

        async.series([
            
            // Create one ad
            (callback) => {
                persistence.create(
                    null,
                    AD1,
                    (err, ad) => {
                        assert.isNull(err);

                        assert.isObject(ad);
                        assert.equal(ad.id, AD1.id);
                        assert.equal(ad.title, AD1.title);
                        assert.equal(ad.text, AD1.text);

                        callback();
                    }
                );
            },
        // Create another ad
            (callback) => {
                persistence.create(
                    null,
                    AD2,
                    (err, ad) => {
                        assert.isNull(err);

                        assert.isObject(ad);
                        assert.equal(ad.id, AD2.id);
                        assert.equal(ad.title, AD2.title);
                        assert.equal(ad.text, AD2.text);

                        callback();
                    }
                );
            },
        // Create yet another ad
            (callback) => {
                persistence.create(
                    null,
                    AD3,
                    (err, ad) => {
                        assert.isNull(err);

                        assert.isObject(ad);
                        assert.equal(ad.id, AD3.id);
                        assert.equal(ad.title, AD3.title);
                        assert.equal(ad.text, AD3.text);

                        callback();
                    }
                );
            },
            // Get all ads
            (callback) => {
                persistence.getPageByFilter(
                    null,
                    new FilterParams(),
                    new PagingParams(),
                    (err, page) => {
                        assert.isNull(err);

                        assert.isObject(page);
                        assert.lengthOf(page.data, 3);

                        callback();
                    }
                );
            },
            // Run proccessing
            (callback) => {
                cleanProcessor.open(null, (err) => {
                    assert.isNull(err);
                    callback();
                });
            },
            (callback) => {
                setTimeout(() => {
                    callback();
                }, 3100);
            },

            // Get all ads
            (callback) => {
                persistence.getPageByFilter(
                    null,
                    new FilterParams(),
                    new PagingParams(),
                    (err, page) => {
                        assert.isNull(err);

                        assert.isObject(page);
                        assert.lengthOf(page.data, 1);
                        assert.equal(page.data[0].id, AD1.id);

                        callback();
                    }
                );
            },
            (callback) => {
                cleanProcessor.close(null, (err) => {
                    assert.isNull(err);
                    callback();
                });
            }

        ],
            done);
    });
});