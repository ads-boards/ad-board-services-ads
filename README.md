# <img src="https://github.com/pip-services/pip-services/raw/master/design/Logo.png" alt="Pip.Services Logo" style="max-width:30%"> <br/> Ads microservice

This is ads microservice base on  Pip.Services library. 
It keeps a list of supported applications that are referenced from other content microservices.

The microservice currently supports the following deployment options:
* Deployment platforms: Standalone Process, Seneca
* External APIs: HTTP/REST
* Persistence: Flat Files, MongoDB

This microservice has no dependencies on other microservices.

## Acknowledgements

This microservice was created and currently maintained by *Levichev Dmitry*.
